---
path: "/blog/my-second-post"
date: "2019-05-24"
title: "Applying Concurrent Engineering"
---

What have Twitter, Pinterest, Reddit, GitHub, Etsy, Tumblr, Spotify, PayPal, Verizon, Comcast, Netflix, Facebook, Twitter, The Guardian, Talk talk, British Airways, Facebook and most recently the UK conservative party all got in common. They are all recipients of security breaches.

From a technology standpoint, we are experiencing rapid, constant change combined with increased complexity and we have reached a point where software professionals are sinking into an insecure quagmire otherwise known as the world wide web. 

As an illustrative example let's look back to a time when front-end developers were typically using jQuery an abstraction of Javascript to build User Interfaces (UI’s) to engage with an ever more sophisticated audience (apparently afflicted by something called "consumerization") and the back end folks were reminding the front end, folks, that they were dependent on the data they were literally able to "serve up". Moving forward to the present day - we have a range of Javascript frameworks such as Reacts.js, Angular.js and Node.js that have made the front-end vs. back-end distinction redundant. The apparent allure of UI centric approaches, combined with a shift towards increased abstraction via tools, libraries or platforms means that there are less of people with a comprehensive bottom-up understanding of the sophisticated tools that we are using. Software Professionals are faced with the challenge of keeping abreast of an ever increasing number of tools, methodologies and languages. We need a fresh look as to how we address these challenges based on the principles of concurrent engineering:

**Concurrent engineering**, also known as simultaneous **engineering**, is a method of designing and developing products, in which the different stages run simultaneously, rather than consecutively. It decreases product development time and also the time to market, leading to improved productivity and reduced costs.

We have lost the battle concerning our capacity to create a secure perimeter to keep the bad people out. We need to consider security and the vulnerabilities of our applications and mitigate the implications of increasingly inevitable security breaches. Software Vendors need to ensure that they are providing the right support regarding training and time to assimilate change. Procurement Professionals must be wary of the ever increasing levels of abstraction and factor in the real cost of ownership. Collectively the adoption of more rigorous engineering principles, adapting the principles of concurrent engineering, to place security at the core of our appropriately engineered product solutions.
